#!/bin/bash
. ~/.vlm-env

function usage ()
{
  echo "Usage: $(basename $0) help | net | vm | template | console | ssh | scp | ansible | iso"
  echo ""
if [ "$2" = "vm" ]
then
  cat <<EOF
    list [vm] [vm]
    show
    create
    delete
    start
    stop
    restart
    clone
    tag
    add
      cpu
      memory
      disk
      interface
      cdrom
      port-forward
    del
      cpu
      memory
      disk
      interface
      cdrom
      port-forward
    modify
      cpu
      memory
      disk
      interface
      cdrom
      port-forward
EOF
elif [ "$2" = "template" ]
then
  cat <<EOF
    list
    show
    create
    delete
    start
    stop
    restart
    clone
    tag
    add
      cpu
      memory
      disk
      interface
      cdrom
    del
      cpu
      memory
      disk
      interface
      cdrom
    modify
      cpu
      memory
      disk
      interface
      cdrom
EOF
elif [ "$2" = "net" ]
then
  cat <<EOF
    list
    show
    create
    delete
    modify
EOF
elif [ "$2" = "iso" ]
then
  cat <<EOF
    list
    show
    add
    delete
    download
EOF
elif [ "$2" = "console" ]
then
  cat <<EOF
  $(basename $0) console (vm | template)

  Example: 
    $(basename $0) console fedora25
    $(basename $0) console server01
EOF
elif [ "$2" = "ssh" ]
then
  cat <<EOF
  $(basename $0) ssh (vm | template)

  Example: 
    $(basename $0) ssh fedora25
    $(basename $0) ssh server01
EOF
elif [ "$2" = "scp" ]
then
  cat <<EOF
  $(basename $0) scp <file> [<user@>](vm | template):[<path>]
  $(basename $0) scp -r <dir> [<user@>](vm | template):[<path>]

  Example: 
    $(basename $0) scp no.file fedora25:
    $(basename $0) scp my.file user@fedora25:/root/my.file
    $(basename $0) scp -r my.dir user@fedora25:/srv
EOF
elif [ "$2" = "ansible" ]
then
  cat <<EOF
  $(basename $0) ansible (vm | template)

  Example: 
    $(basename $0) ansible server01 setup
    $(basename $0) ansible server01 vlm-init
    $(basename $0) ansible server01 vlm-init,nginx
EOF
else
cat <<EOF
  Usage: $(basename $0) help | net | vm | template | console | ssh | scp | ansible | iso

  Example:
    $(basename $0) help full
    $(basename $0) help vm
    $(basename $0) template create fedora25 /srv/fedora25.iso
    $(basename $0) template create centos7 kickstarts/centos.ks
    $(basename $0) console fedora25 
    $(basename $0) vm create server01 fedora25
    $(basename $0) vm create server01 fedora25 nginx
    $(basename $0) ansible server01 setup
    $(basename $0) ansible server01 nginx,mysql
    $(basename $0) ssh server01

EOF
fi
  exit 99
}

cmd_ansible_playbook="$(which ansible-playbook)"
if [ ! -x "$cmd_ansible_playbook" ]
then
  echo "Missing ansible-playbook command, install that first"
  exit 50
fi

if [ "$1" = "vm" ]
then
  if [ "$2" = "create" ]
  then
    if [ "x$3" = "x" ]
    then
      echo Missing vm name, please give me a name of the vm.
      exit 95
    else
      if [ -f "/etc/libvirt/qemu/${3}.xml" ]
      then
        echo Virtual Machine already exists, please choose another name.
        exit 96
      fi
    fi
    if [ "x$4" = "x" ]
    then
      echo Missing template name, please provide one of the following:
      cd /var/lib/libvirt/images; ls template-* | sed -e 's/\.qcow2$//g' -e 's/template\-//g'
      exit 97
    else
      if [ ! -f "/var/lib/libvirt/images/template-${4}.qcow2" ]
      then
        echo "Template does not exist, please provide one of the following:"
        cd /var/lib/libvirt/images; ls template-* | sed -e 's/\.qcow2$//g' -e 's/template\-//g'
        exit 98
      fi
    fi
    VMNAME=$(eval echo $3)
    TEMPLATENAME=$(eval echo $4)
    ANSIBLEROLE=$(eval echo $5)
    VCPU=1
    VMEM=1024
    for VM in $VMNAME
    do
      echo "Creation of the VM $VM has started, cloning template $TEMPLATENAME"
      virsh dominfo ${VM} > /dev/null 2>&1
      if [ $? != 0 ]
      then
        if [ ! -f "/var/lib/libvirt/images/${VM}" ]
        then
          qemu-img create -f qcow2 -b /var/lib/libvirt/images/template-${4}.qcow2 /var/lib/libvirt/images/${VM}.qcow2 > /dev/null 2>&1
        fi
        virt-install --name ${VM} --vcpu ${VCPU} --memory ${VMEM} --disk /var/lib/libvirt/images/${VM}.qcow2 --import --graphics vnc,listen=0.0.0.0 --noautoconsole > /dev/null 2>&1
        echo  "Clone to new VM complete, booting and waiting for an interface to come online."
        until [ $(virsh domifaddr ${VM} | grep -c ipv4) -ge "1" ] ; do sleep 1 ; done
        sleep 3
        echo "Configuring VM." 
        VMIP=$(virsh -q domifaddr $VMNAME | awk '{ gsub("/[0-9]+", ""); print $NF }')
        ssh-keygen -R $VMIP
        ANSIBLE_DEPRECATION_WARNINGS=False ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook /root/bin/ansible/playbook.yml -i "${VMIP}," -e ansible_ssh_pass=${DEFAULT_PASSWORD} -e default_password=${DEFAULT_PASSWORD} -e hostname=${VM} -e fqdn=${FQDN} -e redhat_username=${REDHAT_USERNAME} -e redhat_password=${REDHAT_PASSWORD} --tags vlm-init,$ANSIBLEROLE
        echo Configuration complete
        echo Creation of the VM $VM completed.
      else
        echo $VM already exists.
      fi
    done
  elif [ "$2" = "delete" ]
  then
    if [ ! -z "$3" ]
    then
      if [ "$3" = "all" ]
      then
        VMNAME=$(virsh list --name --all | grep -v template-)
      else
        VMNAME=$(eval echo $3)
      fi
      for VM in $VMNAME
      do
        DISKS=$(virsh dumpxml $VM | grep "\.qcow2" | grep -v template | sed -e "s/.*='//g" -e "s/'.*//g")
        virsh destroy $VM
        virsh undefine $VM
        rm -rf $DISKS
      done
    else
      usage
    fi
  elif [ "$2" = "show" ] || [ "$2" = "list" ]
  then
    if [ ! -z "$3" ]
    then
      VMNAME=$3
      DEFAULT_INTERFACE=$(ip route get 8.8.8.8 | awk '$0 ~ /dev/ { print $NF }')
      let VNCPORT=5900+$(ps -ef | grep ^qemu | grep $VMNAME | sed -e 's/.*\-vnc 0.0.0.0://g' -e 's/ .*//g')
      VMIP=$(virsh -q domifaddr $VMNAME | awk '{ gsub("/[0-9]+", ""); print $NF }')
      virsh dominfo $VMNAME
      echo "IP-address: $VMIP"
      echo "Console: $DEFAULT_INTERFACE:$VNCPORT"
    else
      virsh list --title --all | grep -v template-
    fi
  elif [ "$2" = "import" ]
  then
    if [ ! -z "$3" ]
    then
      VMNAME=$(eval echo $3)
      VCPU=1
      VMEM=1024
      for VM in $VMNAME
      do
	if [ ! -f "/var/lib/libvirt/images/${VM}" ]
	then
	  #qemu-img create -f qcow2 -b /var/lib/libvirt/images/template-rhel71.qcow2 /var/lib/libvirt/images/${VM}.qcow2
	  echo Image does not exist and nothing specified, please copy it to /var/lib/libvirt/images
	fi
	virt-install --name ${VM} --vcpu ${VCPU} --memory ${VMEM} --disk /var/lib/libvirt/images/${VM}.qcow2 --import --noautoconsole
	DEFAULT_INTERFACE=$(ip route get 8.8.8.8 | awk '$0 ~ /dev/ { print $NF }')
	let VNCPORT=5900+$(ps -ef | grep ^qemu | grep $VMNAME | sed -e 's/.*\-vnc 0.0.0.0://g' -e 's/ .*//g')
	echo Connect to $VMNAME by using a vnc-viewer and connecting to: $DEFAULT_INTERFACE:$VNCPORT
      done
    else
      usage
    fi
  elif [ "$2" = "start" ]
  then
    VMNAME=$(eval echo $3)
    virsh start $VMNAME
  elif [ "$2" = "stop" ]
  then
    VMNAME=$(eval echo $3)
    virsh destroy $VMNAME
  elif [ "$2" = "restart" ]
  then
    VMNAME=$(eval echo $3)
    virsh destroy $VMNAME
    virsh start $VMNAME
  elif [ "$2" = "clone" ]
  then
    VMNAME=$(eval echo $3)
    echo not implemented yet
  elif [ "$2" = "tag" ] || [ "$2" = "label" ]
  then
    VMNAME=$(eval echo $3)
    echo not implemented yet
  elif [ "$2" = "add" ]
  then
    VMNAME=$(eval echo $4)
    if [ "$3" = "cpu" ]
    then
      AMOUNT=$5
      virsh setvcpus $VMNAME --count $AMOUNT --config 
      virsh destroy $VMNAME
      virsh start $VMNAME
      virsh setvcpus $VMNAME --count $AMOUNT --live
    elif [ "$3" = "memory" ]
    then
      AMOUNT=$5
      virsh setmaxmem $VMNAME ${AMOUNT} --config
      virsh setmem $VMNAME ${AMOUNT} --config
      virsh destroy $VMNAME
      virsh start $VMNAME
      virsh setmem $VMNAME ${AMOUNT} --live
    elif [ "$3" = "disk" ]
    then
      DISKSIZE=${5}
      DISKSIZE=${DISKSIZE:-20}
      virsh destroy $VMNAME
      DISKS=$(virsh dumpxml $VMNAME | grep "\.qcow2" | grep -v template | sed -e "s/.*='//g" -e "s/'.*//g")
      DISKCOUNT=$(echo $DISKS | wc -w)
      VMDISK=$(echo $DISKS | sed 's/ /\n/g' | sort | tail -1 | sed "s/\.qcow2/-Disk${DISKCOUNT}\.qcow2/g")
      #echo VM: $VMNAME DISKS: $DISKS DISKCOUNT: $DISKCOUNT SIZE: $DISKSIZE VMDISK: $VMDISK
      qemu-img create -f qcow2 ${VMDISK} ${DISKSIZE}g
      vmdisk=a
      vmdisk=$(echo $vmdisk | tr "0-9a-z" "${DISKCOUNT}-9a-z_")
      virsh attach-disk $VMNAME --source ${VMDISK} --target hd$vmdisk --targetbus ide --subdriver qcow2 --persistent
      virsh start $VMNAME
    elif [ "$3" = "interface" ]
    then
      echo
    elif [ "$3" = "cdrom" ]
    then
      echo
    elif [ "$3" = "port-forward" ]
    then
      echo
    else
      usage
    fi
  elif [ "$2" = "del" ]
  then
    VMNAME=$(eval echo $4)
    if [ "$3" = "cpu" ]
    then
      echo
    elif [ "$3" = "memory" ]
    then
      echo
    elif [ "$3" = "disk" ]
    then
      virsh detach-disk $VMNAME hdb --persistent
      echo
    elif [ "$3" = "interface" ]
    then
      echo
    elif [ "$3" = "cdrom" ]
    then
      echo
    elif [ "$3" = "port-forward" ]
    then
      echo
    else
      usage
    fi
  elif [ "$2" = "modify" ]
  then
    VMNAME=$(eval echo $3)
    if [ "$3" = "cpu" ]
    then
      echo
    elif [ "$3" = "memory" ]
    then
      echo
    elif [ "$3" = "disk" ]
    then
      echo
    elif [ "$3" = "interface" ]
    then
      echo
    elif [ "$3" = "cdrom" ]
    then
      echo
    elif [ "$3" = "port-forward" ]
    then
      echo
    else
      usage
    fi
  fi
elif [ "$1" = "net" ]
then
  echo GAH
  if [ "$2" = "port-forward" ]
  then
    if [ "x$3" = "x" ]
    then
      echo "Missing VM name"
      exit 70
    fi 
    if [ "x$4" = "x" ]
    then
      echo "Missing port"
      exit 71
    fi 
    VMNAME=$(eval echo $3)
    VMIP=$(virsh -q domifaddr $VMNAME | awk '{ gsub("/[0-9]+", ""); print $NF }')
    PORT=$(eval echo $4)
    HOSTIP=$(ip route get 8.8.8.8 | awk '$0 ~ /dev/ { print $NF }')
    iptables -t nat -A PREROUTING -d ${HOSTIP} -p tcp --dport $PORT -j DNAT --to ${VMIP}:$PORT
    iptables -I FORWARD -d ${VMIP}/32 -p tcp -m state --state NEW -m tcp --dport $PORT -j ACCEPT
    echo "Creating port-forward from $HOSTIP:$PORT to $VMNAME:$PORT"
  fi
elif [ "$1" = "templates" ] || [ "$1" = "template" ]
then
  if [ "$2" = "create" ]
  then
    if [ "x$3" = "x" ]
    then
      echo Missing template name, please give me a name of the template.
      exit 95
    else
      if [ -f "/etc/libvirt/qemu/template-${3}.xml" ]
      then
        echo Template already exists, please choose another name.
        exit 96
      fi
    fi
    if [ "x$4" = "x" ]
    then
      echo Missing source for template installation, please specify an ISO or kickstart file to use for the template.
      exit 94
    else
      if [ "$(echo $4 | grep -ic '.iso')" -eq "1" ]
      then
        TEMPLATE_SOURCE=ISO
        if [ ! -f "$4" ]
        then
          echo Missing ISO, the specified ISO does not exist in $4.
          exit 93
        fi
      elif [ "$(echo $4 | grep -ic 'preseed.cfg')" -eq "1" ]
      then
        TEMPLATE_SOURCE=PRESEED
        if [ ! -f "$4" ]
        then
          echo Missing preseed file, the specified preseed file does not exist in $4.
          exit 89
        fi
      elif [ "$(echo $4 | grep -ic '.ks')" -eq "1" ]
      then
        TEMPLATE_SOURCE=KICKSTART
        if [ ! -f "$4" ]
        then
          echo Missing kickstart file, the specified kickstart file does not exist in $4.
          exit 92
        fi
        if [ "x$5" = "x" ]
        then
          if [ "$(echo $4 | grep -ic centos)" -ge "1" ]
          then
            echo You need to provide a mirror which we will use for kickstart, maybe this is suitable for you:
            echo
            echo "  $(basename $0) template create $3 $4 $(curl -s 'http://mirrorlist.centos.org/?repo=os&arch=x86_64&release=7' | head -n1)"
            exit 91
          elif [ "$(echo $4 | grep -ic fedora)" -ge "1" ]
          then
            echo You need to provide a mirror which we will use for kickstart, maybe this is suitable for you:
            echo
            echo "  $(basename $0) template create $3 $4 $(curl -s 'https://mirrors.fedoraproject.org/mirrorlist?repo=fedora-29&arch=x86_64' | head -n2 | tail -n1)"
            exit 91
          else
            echo Do not understand what OS this is, exiting. Please provide a OS which this scripts understands what it is.
            exit 90
          fi
        fi
      fi
    fi
    echo "Creating template"
    TEMPLATENAME=$(eval echo $3)
    ISO=$(eval echo $4)
    KICKSTART=$(eval echo $4)
    KICKSTART_URL=$(eval echo $5)
    VCPU=1
    VMEM=1500
    VDISK=20G
    if [ ! -f "/var/lib/libvirt/images/template-${TEMPLATENAME}.qcow2" ]
    then
      qemu-img create -f qcow2 /var/lib/libvirt/images/template-${TEMPLATENAME}.qcow2 ${VDISK}
    fi
    if [ "${TEMPLATE_SOURCE}" = "ISO" ]
    then
      virt-install --name template-${TEMPLATENAME} --vcpu ${VCPU} --memory ${VMEM} --disk /var/lib/libvirt/images/template-${TEMPLATENAME}.qcow2 --graphics vnc,listen=0.0.0.0 --noautoconsole --cdrom $ISO
    elif [ "${TEMPLATE_SOURCE}" = "PRESEED" ]
    then
      virt-install --name template-${TEMPLATENAME} --vcpu ${VCPU} --memory ${VMEM} --disk /var/lib/libvirt/images/template-${TEMPLATENAME}.qcow2 --graphics vnc,listen=0.0.0.0 --noautoconsole --location "http://ftp.se.debian.org/debian/dists/testing/main/installer-amd64" --initrd-inject=${KICKSTART} --extra-args="preseed/file=/$(echo ${KICKSTART} | sed 's#.*/##g') preseed/file/checksum=$(md5sum ${KICKSTART} | awk '{ print $1 }') auto=true"
      #--disk=pool=default,size=5,format=qcow2,bus=virtio
    elif [ "${TEMPLATE_SOURCE}" = "KICKSTART" ]
    then
      virt-install --name template-${TEMPLATENAME} --vcpu ${VCPU} --memory ${VMEM} --disk /var/lib/libvirt/images/template-${TEMPLATENAME}.qcow2 --graphics vnc,listen=0.0.0.0 --noautoconsole --location "${KICKSTART_URL}" --initrd-inject=${KICKSTART} --extra-args "inst.ks=file:/$(echo ${KICKSTART} | sed 's#.*/##g')"
    fi
  elif [ "$2" = "list" ]
  then
    echo "The following templates are available:"
    cd /var/lib/libvirt/images; ls template-* | sed -e 's/\.qcow2$//g' -e 's/template\-//' | sort
  elif [ "$2" = "delete" ]
  then
    if [ ! -z "$3" ]
    then
      if [ "$3" = "all" ]
      then
        TEMPLATENAME=$(cd /var/lib/libvirt/images; ls template-* | sed -e 's/\.qcow2$//g')
      else
        TEMPLATENAME=$(eval echo $3)
      fi
      for TEMPLATE in $TEMPLATENAME
      do
        DISKS=$(virsh dumpxml template-$TEMPLATE | grep "\.qcow2" | sed -e "s/.*='//g" -e "s/'.*//g")
        virsh destroy template-$TEMPLATE
        virsh undefine template-$TEMPLATE
        rm -rf $DISKS
      done
    else
      usage
    fi

  else
    usage
  fi
elif [ "$1" = "ansible" ]
then
  VMNAME=$(eval echo $2)
  VMIP=$(virsh -q domifaddr $VMNAME | awk '{ gsub("/[0-9]+", ""); print $NF }')
  ANSIBLE_DEPRECATION_WARNINGS=False ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook /root/bin/ansible/playbook.yml -i ${VMIP}, -e ansible_ssh_pass=${DEFAULT_PASSWORD} -e default_password=${DEFAULT_PASSWORD} -e fqdn=${FQDN} -e hostname=${VMNAME} --tags vlm-init,$3
  # freebsd needs some more stuff to function -e ansible_python_interpreter=/usr/local/bin/python2.7, figure out how to do it
elif [ "$1" = "ssh" ]
then
  if [ ! -z "$2" ]
  then
    VMNAME=$(eval echo $2)
    VMNAME=$(virsh list --name | grep -ie ${VMNAME}\.*)
    VMIP=$(virsh -q domifaddr $VMNAME | awk '{ gsub("/[0-9]+", ""); print $NF }')
    ssh root@$VMIP
  else
    usage
  fi
elif [ "$1" = "console" ]
then
  if [ ! -z "$2" ]
  then
    VMNAME=$(eval echo $2)
    DEFAULT_INTERFACE=$(ip route get 8.8.8.8 | awk '$0 ~ /dev/ { print $NF }')
    let VNCPORT=5900+$(ps -ef | grep ^qemu | grep $VMNAME | sed -e 's/.*\-vnc 0.0.0.0://g' -e 's/ .*//g')
    VMIP=$(virsh -q domifaddr $VMNAME | awk '{ gsub("/[0-9]+", ""); print $NF }')
    echo $VMNAME $DEFAULT_INTERFACE:$VNCPORT $VMIP
  else
    usage
  fi
else
  usage $@
fi
