Virtual Lab Manager

This is a project I created to workaround limitations with virt-manager.

I set up new machines frequently since I test different situations that my customers have and do some labs for my own. Due to this I cannot be bothered with spending time setting the same thing up over and over. Ansible is awesome to be used for these systems and something I recommend to use.

How to use it:

Setup a config file in ~/.vlm-env

```
# cat ~/.vlm-env 
DEFAULT_PASSWORD=redhat123
REDHAT_USERNAME=myusername
REDHAT_PASSWORD=mypassword
FQDN=example.com
# If you want to make your ansible output a bit more fancy, use the following
export ANSIBLE_STDOUT_CALLBACK=unixy
```

The above config file is imported when you run the command. You can then simply run the command with:

```
# ./virt-lab-manager
```

or

```
# ./vlm
```


